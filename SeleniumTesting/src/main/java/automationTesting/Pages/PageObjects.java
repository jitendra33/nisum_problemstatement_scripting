package automationTesting.Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.asserts.SoftAssert;

public class PageObjects 
{
	WebDriver driver;
	@FindBy(how=How.XPATH,using="(//li[contains(@class,'dropDown hoverFade category-')])[1]")
		WebElement mainnav_cookware;
	@FindBy(how=How.XPATH,using="//nav[@id='topnav-container']//descendant::ul[2]//li[12]//a")
	WebElement tea_kettlelink; 
	@FindBy(how=How.XPATH,using="(//span[@class='product-thumb-casing'])[1]")
	WebElement tea_kettleprod; 
	@FindBy(how=How.XPATH,using="//button[contains(@id,'primaryGroup_addToCart')]")
	WebElement addtocart; 
	
	@FindBy(how=How.XPATH,using="//a[@id='anchor-btn-checkout']")
	WebElement checkout;
	
	@FindBy(how=How.XPATH,using="//ul[@class='aligned-text-list']//li[1]//a")
	WebElement saveforlater;
	
	@FindBy(how=How.XPATH,using="//div[@class='cart-table-row-title']")
	WebElement savedproduct; 
	
	@FindBy(how=How.XPATH,using="//h2[@class='save-for-later']")
	WebElement savedproductsectionheading; 
	
	@FindBy(how=How.XPATH,using="//a[@class='view-cart']//span[2]")
	WebElement noOfprodinCart; 
	
	public PageObjects(WebDriver driver)
	{
	this.driver=driver;
	
	}
	public void savetheproduct() throws Exception
	{
	
		Actions act=new Actions(driver);
		System.out.println("Mouse hovering on mannavivation menu bar-cookware");
		act.moveToElement(mainnav_cookware).build().perform();
		
			System.out.println("Clicking on Tea Kettles under Cookware");
			tea_kettlelink.click();
			System.out.println("Adding a Tea Kettle in cart");
			tea_kettleprod.click();
			System.out.println("Click on add to cart");
			
			addtocart.click();
			WebDriverWait wait =new WebDriverWait(driver,20);
			wait.until(ExpectedConditions.visibilityOf(checkout));
			System.out.println("Total number of window are:"+driver.getWindowHandles().size());
			System.out.println("Click on checkout");
			checkout.click();
			System.out.println("Clicked on save for later");
			saveforlater.click();
			System.out.println("Saved Product name is:"+savedproduct.getText());
			System.out.println("Product saved status is:"+savedproduct.isDisplayed());
			SoftAssert asrt=new SoftAssert();
			asrt.assertTrue(savedproduct.isDisplayed(), "Validating whether product is saved or not");
			System.out.println("Saved Product Section Heading is:"+savedproductsectionheading.getText());
		
			
	}

}
