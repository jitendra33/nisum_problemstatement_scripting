package automationTesting.SeleniumTesting;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import automationTesting.Pages.PageObjects;

public class TestScript 
{
	WebDriver driver;
	FileInputStream fis;
	Properties pro;
	@BeforeClass 
	public void setup()
	{
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		driver =new ChromeDriver();
		
		 
	}
	@Test(priority=0)
	public void launchappurl() throws Exception 
	{
		System.out.println("Launching the application url by reading the application url from property file and then  maximizing the window");
		fis=new FileInputStream("./src/main/prop.properties");
		 pro=new Properties();
		 pro.load(fis);
		 driver.get(pro.getProperty("applicationurl"));
			driver.manage().window().maximize();
		
	}
	@Test(priority=1)
	public void testdemo() throws Exception
	{
		System.out.println("Initializing the locators");
		PageObjects obj=PageFactory.initElements(driver,PageObjects.class);
		obj.savetheproduct();
	}
	@AfterClass 
	public void closebrowser() throws Exception
	{
		
		driver.quit();
		System.out.println("Closed the browser");
	}
	
	
	
	
}
